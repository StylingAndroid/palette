package com.stylingandroid.palette;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.graphics.Palette;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class PaletteActivity extends Activity {
    private static final int SELECT_IMAGE = 1;
    public static final String IMAGE_MIME_TYPE = "image/*";

    private ImageView imageView;
    private View paletteOverlay;
    private View normalVibrant;
    private View lightVibrant;
    private View darkVibrant;
    private View normalMuted;
    private View lightMuted;
    private View darkMuted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_palette);
        imageView = (ImageView) findViewById(R.id.image);
        paletteOverlay = findViewById(R.id.palette_overlay);
        normalVibrant = findViewById(R.id.normal_vibrant);
        lightVibrant = findViewById(R.id.light_vibrant);
        darkVibrant = findViewById(R.id.dark_vibrant);
        normalMuted = findViewById(R.id.normal_muted);
        lightMuted = findViewById(R.id.light_muted);
        darkMuted = findViewById(R.id.dark_muted);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.palette, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_open) {
            Intent intent = new Intent();
            intent.setType(IMAGE_MIME_TYPE);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent,
                    getString(R.string.select_image)), SELECT_IMAGE);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == SELECT_IMAGE) {
            Uri selectedImageUri = data.getData();
            loadBitmap(selectedImageUri);
        }
    }

    private void loadBitmap(Uri selectedImageUri) {
        ImageLoadTask imageLoadTask = ImageLoadTask.newInstance(this);
        imageLoadTask.execute(selectedImageUri);
    }

    public void setBitmap(Bitmap bitmap) {
        imageView.setImageBitmap(bitmap);
        Palette.generateAsync(bitmap, PaletteListener.newInstance(this));
    }

    public void setPalette(Palette palette) {
        if (palette == null) {
            hidePalette();
            return;
        }
        paletteOverlay.setVisibility(View.VISIBLE);
        setSwatch(normalVibrant, palette.getVibrantSwatch());
        setSwatch(lightVibrant, palette.getLightVibrantSwatch());
        setSwatch(darkVibrant, palette.getDarkVibrantSwatch());
        setSwatch(normalMuted, palette.getMutedSwatch());
        setSwatch(lightMuted, palette.getLightMutedSwatch());
        setSwatch(darkMuted, palette.getDarkMutedSwatch());
    }

    private void hidePalette() {
        paletteOverlay.setVisibility(View.GONE);
    }

    private void setSwatch(View view, Palette.Swatch swatch) {
        if (swatch == null) {
            view.setVisibility(View.INVISIBLE);
        } else {
            view.setVisibility(View.VISIBLE);
            view.setBackgroundColor(swatch.getRgb());
        }
    }

    public void showError(int errorId) {
        String error = getString(errorId);
        Toast.makeText(this, error, Toast.LENGTH_LONG).show();
    }
}
