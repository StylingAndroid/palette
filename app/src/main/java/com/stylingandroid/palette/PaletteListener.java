package com.stylingandroid.palette;

import android.support.v7.graphics.Palette;

import java.lang.ref.WeakReference;

class PaletteListener implements Palette.PaletteAsyncListener {
    private final WeakReference<PaletteActivity> activityWeakReference;

    public static PaletteListener newInstance(PaletteActivity activity) {
        WeakReference<PaletteActivity> activityWeakReference = new WeakReference<PaletteActivity>(activity);
        return new PaletteListener(activityWeakReference);
    }

    PaletteListener(WeakReference<PaletteActivity> activityWeakReference) {
        this.activityWeakReference = activityWeakReference;
    }

    @Override
    public void onGenerated(Palette palette) {
        PaletteActivity activity = activityWeakReference.get();
        if (activity != null) {
            activity.setPalette(palette);
        }
    }
}
